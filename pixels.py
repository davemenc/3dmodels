from PIL import Image
class Pixels:
	def __init__(self,fname):
		self.im = Image.open(fname)
		self.width,self.height = self.im.size
		self.pixels = {}
		self.mode = self.im.mode
		self.fname = fname
		self.maxval = -1
		self.minval = 3000000000
		#dprint(self.fname,self.mode)
		for y in range(0,self.height):
			for x in range(0,self.width):

				if self.mode == "P" or self.mode == "L":
					pix = self.im.getpixel((x,y))
				elif self.mode == "RGB":
					rgb = self.im.getpixel((x,y))
					pix = int((rgb[0]+rgb[1]+rgb[2])/3)
				else:
					pix = self.im.getpixel((x,y))
				self.pixels[(x,y)] = pix
				if pix>self.maxval:
					self.maxval = pix
				if pix<self.minval:
					self.minval = pix
	def fetch_pixel(self,x,y):
		idx = (x,y)
		if idx in self.pixels:
			return self.pixels[idx]
		else:
			return 0
	def write_pixels(self,fname):
		with open(fname,"wt") as f:
			for y in range(0,self.height):
				for x in range(0,self.width):
					f.write(str(self.fetch_pixel(x,y)))
					if x<self.width-1:
						f.write("\t")
				f.write("\n")

if __name__ == "__main__":
	
	#p1 = Pixels("images/squares.png")
	#maxz = -1

	#for x in range(0,p1.width):
	#	for y in range(0,p1.height):
	#		val = p1.fetch_pixel(x,y)
	#		if val>maxz:
	#			maxz = val
	#print(maxz)
	#p1.write_pixels("squares_pix.tsv")

	#p3 = Pixels("images/square_grey.png")
	#p3.write_pixels("square_grey_pix.tsv")
	#p4 = Pixels("images/tiki100.png")
	#p4.write_pixels("images/tiki100.tsv")
	"""
	p2 = Pixels("images/rawsquares.png")
	for x in range(0,p2.width):
		for y in range(0,p2.height):
			val = p2.fetch_pixel(x,y)

	p2.write_pixels("rawsquares_pix.tsv")
	"""