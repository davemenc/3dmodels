import math 
def volume_d(d,h):
	return volume_r(d/2,h)
def volume_r(r,h):
	return r*r*h*math.pi
goal_v = 60000
max_error = .01
max_goal = goal_v+goal_v*max_error
min_goal = goal_v-goal_v*max_error
min_w = 30
max_w = 60 
min_h = 30
max_h = 100

print(f"// goal: {goal_v} +/- {max_error}: {min_goal/1000} - {max_goal/1000}")
print(f"//width: {min_w} - {max_w}")
print(f"//height: {min_h} - {max_h}")
print(f"//volumes: {round(volume_d(min_w,min_h)/1000,0)} - {round(volume_d(max_w,max_h)/1000,0)}")
print("/*")
for w in range(min_w,max_w):
	for h in range(min_h,max_h):
		v = volume_d(w,h)
		if v<max_goal and v>min_goal and h>w and w*1.5>h:
			#print(w,h,round(v,2))
			print(f"//volume: {v}\nw={w};\nh={h};\n")
print("*/\n//",end="")