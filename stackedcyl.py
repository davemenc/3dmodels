#gen_height_map.py
from pixels import Pixels 
vase_name = "classic"
vase_name = "doublegourd"
vase_name = "mod"
vase_name = "face2"
vase_name = "face1"
vase_name = "davehead"
vase_name = "bald"
vase_name = "bald2"
vase_name = "weird"
vase_name = "lookingup"

img_name = "vases/"+vase_name+".png"
openscadfile = "vases/"+vase_name+".scad"
#img_name = "vase2.png"
#img_name = "vase1.png"
img_name = "images/egg.png"
p = Pixels(img_name)
print(f"from {p.fname} in mode {p.mode} we get {p.width}X{p.height}; maxval:{p.maxval}; minval: {p.minval} ")
print(f"ouput file: {openscadfile}")
widths = []
for y in range(p.height):
	first = -1
	last = -1
	for x in range(p.width):
		pix = p.fetch_pixel(x,y)
		if pix == 0:
			if first==-1:
				first = x
			if x>last:
				last=x
		if first>-1 and last>-1 and pix>0:
			break
	width = last-first+1
	#print(first,last,width)
	if width > 20 :
		widths.append(width)
print("total values: ",len(widths))
#print(widths)
with open(openscadfile,"wt") as f:
	first = True
	f.write(f"widths = [")
	for w in widths:
		if not first:
			f.write(",")
		else:
			first=False
		f.write(str(w))
	f.write("];")

