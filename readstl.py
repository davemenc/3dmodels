import random
import time


class Vector:
    """ Point class represents and manipulates x,y,z coords. """

    def __init__(self,x=None,y=None,z=None):
        """ Create a new point at the origin """
        if x is not None:
            self.x = float(x)
        if y is not None:
            self.y = float(y)
        if z is not None:
            self.z = float(z)
           
    def string(self):
            return "Vector: {0}, {1}, {2}".format(self.x,self.y,self.z)

    def dot(self,b):
    #""" self & b are vectors; it reuturns a scalar
            return self.x*b.x+self.y*b.y+self.z*b.y

    def normalise(self):
    # self is a vector; 
            return self/sqrt(dot(self,self))
            
    def cross(self,b):
    #self and b are both vectors; it returns a vector
            #(a2b3  -   a3b2,     a3b1   -   a1b3,     a1b2   -   a2b1)
            x = self.y * b.z - self.z * b.y
            y = self.z * b.z - self.x * b.z
            z = self.x * b.y - self.y * b.x 
            result = Vector(x,y,z)
            return result

###########################
class Polygon:
    """ Polygon class represents and manipulates 3 sided polygons."""

    def __init__(self,p1=None,p2=None,p3=None,normal=None):
        self.normal = normal
        self.bound_xy = [] #should have 4 vects which are all on the same z plane and
        """ create a new facet consiting of 3 points"""
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

        self.max_x = None
        self.max_y = None
        self.max_z = None
        self.min_x = None
        self.min_y = None
        self.min_z = None

    def add_vertex(self,v):
        if self.p1 is None:
            self.p1 = v
        elif self.p2 is None:
            self.p2 = v
        elif self.p3 is None:
            self.p3 = v
        else:
            print "**** too many vertexes in a facet ****"

    def string(self):
        return "Poly: {0}; {1}; {2}".format(self.p1.string(), self.p2.string(), self.p3.string())

    def ray_in_poly(self,x,y):
        """ pt is a vertical ray; does it pass through the poly?"""
        """ note that a vertical ray has no z so we ignore z value of pt"""
        self.calculate()

        #this is a bounding box test -- if it fails we KNOW point isn't in polygon
        if x<self.min_x or x>self.max_x or y<self.min_y or y>self.max_y:
            return False
        #for now let's stop here
        return True

    def point_height(self,x,y):
        """we have a point in the poly: what is the z value?"""
        if self.p1.z == self.p2.z:
            z1 = self.p1.z
            z4 = self.p1.z
        else: 
            z1 = x*abs(self.p1.x-self.p2.x)/abs(self.p1.z-self.p2.z)
            z4 = y*abs(self.p1.y-self.p2.y)/abs(self.p1.z-self.p2.z)
        if self.p1.z == self.p3.z:
            z2 = self.p1.z
            z5 = self.p1.z
        else:
            z2 = x*abs(self.p1.x-self.p3.x)/abs(self.p1.z-self.p3.z)
            z5 = y*abs(self.p1.y-self.p3.y)/abs(self.p1.z-self.p3.z)
        if self.p2.z == self.p3.z:
            z3 = self.p3.z
            z6 = self.p3.z
        else:
            z3 = x*abs(self.p2.x-self.p3.x)/abs(self.p2.z-self.p3.z)
            z6 = y*abs(self.p2.y-self.p3.y)/abs(self.p2.z-self.p3.z)
        z = (z1+z2+z3+z4+z5+z6)/6
        return z

    def calcZ(self, x, y):
        #print self.string(),x,y
        det = (self.p2.y - self.p3.y) * (self.p1.x - self.p3.x) + (self.p3.x - self.p2.x) * (self.p1.y - self.p3.y)
        if det == 0:
            det = 4.9e-324

        l1 = ((self.p2.y - self.p3.y) * (x - self.p3.x) + (self.p3.x - self.p2.x) * (y - self.p3.y)) / det
        l2 = ((self.p3.y - self.p1.y) * (x - self.p3.x) + (self.p1.x - self.p3.x) * (y - self.p3.y)) / det
        l3 = 1.0 - l1 - l2

        return l1 * self.p1.z + l2 * self.p2.z + l3 * self.p3.z


    def calculate(self):
        """ the polygon has some things like it's normal and it's bounding box that need to be calculated """
#        if self.max_x is None:
        self.max_x = max(self.p1.x,self.p2.x,self.p3.x)
        self.max_y = max(self.p1.y,self.p2.y,self.p3.y)
        self.max_z = max(self.p1.z,self.p2.z,self.p3.z)
        self.min_x = min(self.p1.x,self.p2.x,self.p3.x)
        self.min_y = min(self.p1.y,self.p2.y,self.p3.y)
        self.min_z = min(self.p1.z,self.p2.z,self.p3.z)
        #optionally, calculate the normal and compare it to the normal in the poly, if there is one
	        

##########################
class Solid:
    """ A collection of facets (polygons) that presumably make up a solid """
    def __init__(self,name="",normal=None):
        self.min_max = {'minx':2e308, 'maxx':-2e308,'miny':2e308, 'maxy':-2e308,'minz':2e308, 'maxz':-2e308,} 
        self.facet_list = []
        self.name = name
        self.face_count = 0
        
    def add_facet(self,facet):
        facet.calculate()
        self.facet_list.append(facet)
        self.face_count += 1
        self.min_max['minx'] = min(self.min_max['minx'],facet.min_x)
        self.min_max['miny'] = min(self.min_max['miny'],facet.min_y)
        self.min_max['minz'] = min(self.min_max['minz'],facet.min_z)
        self.min_max['maxx'] = max(self.min_max['maxx'],facet.max_x)
        self.min_max['maxy'] = max(self.min_max['maxy'],facet.max_y)
        self.min_max['maxz'] = max(self.min_max['maxz'],facet.max_z)

    def string(self):
        s = "Solid: {0} \n".format(self.name)
        for f in self.facet_list:
            s += f.string()+"\n"
        s += "Faces: {}\n".format(self.face_count)
        s += "\t\tX\t\tY\t\tZ\n"
        s += "Min:\t\t{0}\t{1}\t{2}\n".format(self.min_max['minx'],self.min_max['miny'],self.min_max['minz'])
        s += "Max:\t\t{0}\t{1}\t{2}\n".format(self.min_max['maxx'],self.min_max['maxy'],self.min_max['maxz'])
        s += "Dim:\t\t{0}\t{1}\t{2}\n".format(abs(self.min_max['minx']-self.min_max['maxx']),
                                              abs(self.min_max['miny']-self.min_max['maxy']),
                                              abs(self.min_max['minz']-self.min_max['maxz']))
        
        return s

    
    
##########################
def tokenize(s):
    s = s.strip()
    stoks = s.split(" ")
    ttoks = []
    for t in range(0,len(stoks)):
        stoks[t] = stoks[t].strip()
        if len(stoks[t])>0:
            ttoks.extend(stoks[t].split("\t"))
    rtoks = []
    for t in range(0,len(ttoks)):
        ttoks[t]=ttoks[t].strip()
        if len(ttoks[t])>0:
            rtoks.append(ttoks[t])
    return rtoks

def concat_list(l):
    r=""
    for s in l:
       r += s+" "
    return r

def str_to_float(s):
    return float(s)

def parseline(line,state):
    #print line,state
    # check for blank line
    if len(line)<3:
        return state

    # tokenize the line
    toks = tokenize(line)
    #if we don't have the name of the solid, look for it
    if not state['solid']:
        if toks[0].lower()=='solid':
            del(toks[0])
            state['sname']=concat_list(toks)
            state['solid'] = True
            state['object'] = Solid(state['sname'])
            return state
        else:
            return state

    #if we don't have the facet keyword, look for it
    if not state['facet']:
        if toks[0].lower()== 'facet':
            state['facet'] = True
            if toks[1].lower() == 'normal': # we have normal info
                normal = Vector(str_to_float(toks[2]),str_to_float(toks[3]),str_to_float(toks[4]))
                state['poly']=Polygon(None,None,None,normal)
                
            return state
        else:
            return state

    #if we don't have the outer loop  keywords, look for it
    if not state['outer']:
        if toks[0].lower()=='outer':
            state['outer'] = True
        else:
            return state

    #if we don't have the vertexes
    if state['vertex'] < 3:
        if toks[0].lower() == 'vertex':
            state['poly'].add_vertex(Vector(str_to_float(toks[1]),str_to_float(toks[2]),str_to_float(toks[3])))
            state['vertex'] += 1
            return state
        else:
            return state

    #if we have the endloop, consume it
    if toks[0] == 'endloop':
        return state
    #if we have endfacet, we're done with this facet
    if toks[0] == 'endfacet':
        state['facet'] = False
        state['outer'] = False
        state['vertex'] = 0
        state['poly'].calculate()
        state['object'].add_facet(state['poly'])
    return state

def cvt_pix_2_coord(pix,min_coord,inc):
    """#need a way to convert a h,v into an x,y  # x = float(h)*horizontal_inc+minx"""
    return float(pix) * inc + min_coord

def cvt_coord_2_pix(coord,min_coord,inc):
    """ # need a way to convert a raw x,y to a pix# in h and v  # h = int((x-minx)/horizontal_inc) """
    return int((coord - min_coord)/inc)

        
def main():
    print "Start: {} seconds".format( round(time.clock(),3))
    MAXLINES = 200000
    RAY_HORIZONTAL = 20#count of rays in horizontal
    RAY_VERTICAL  = 20 #count of rays in vertical
    inname = "cube.stl"
    inname = "rotated_cube.stl"
    outdataname = "testdata.txt"
    outstlname = "test.stl"
    outimage = "test.jpg"
    

    state = {'solid':False,'facet':False,'outer':False,'vertex':0,'sname':""}
    print "Input STL: {0}\nOutput Data: {1}\nOutput STL: {2} ({4} lines)\nOutput Image: {3}\nResolution: {5}x{6}".format(inname,outdataname,outstlname,outimage,MAXLINES,RAY_HORIZONTAL,RAY_VERTICAL)

    linecount = 0
    with open(inname, 'rt') as f:
        for line in f:
            state = parseline(line,state)
            #print linecount,line,state
            linecount += 1
            if linecount > MAXLINES:
                break

    print "\n\n************** \n\n File parse complete {0} lines, {1} seconds".format(linecount, round(time.clock(),3))


######################## FILE'S READ ########################
    #print state['object'].string()
    solid = state['object']
    maxx = solid.min_max['maxx']
    minx = solid.min_max['minx']
    maxy = solid.min_max['maxy']
    miny = solid.min_max['miny']
    minz = solid.min_max['minz']
    maxz = solid.min_max['maxz']
    z_range = abs(minz-maxz)
    print "z_range:{}".format(z_range)

    horizontal_inc = abs(maxx-minx)/RAY_HORIZONTAL
    vertical_inc = abs(maxy-miny)/RAY_VERTICAL
    minh = 0
    maxh = int((maxx-minx)/horizontal_inc)
    minv = 0
    maxv = int((maxy-miny)/vertical_inc)
    print "minh:{},maxh: {}, minv: {}, maxv: {}, R_H: {}, R_V: {}".format(minh,maxh, minv,maxv,RAY_HORIZONTAL,RAY_VERTICAL)

    print "width: {}, Height: {}, rayh:{}, rayv: {} hinc: {}, vinc: {}".format(abs(maxx-minx),
                                                                               abs(maxy-miny),
                                                                               RAY_HORIZONTAL,
                                                                               RAY_VERTICAL,
                                                                               horizontal_inc,
                                                                               vertical_inc)

    heights = [[0 for x in range(RAY_VERTICAL+1)] for y in range(RAY_HORIZONTAL+1)]
    print "MINZ=",minz
    for facet in solid.facet_list:
        f_minh = int((facet.min_x-minx)/horizontal_inc)
        f_maxh = int((facet.max_x-minx)/horizontal_inc)+1
        if f_maxh > maxh:
            f_maxh = maxh
        f_minv = int((facet.min_y-miny)/vertical_inc)
        f_maxv = int((facet.max_y-miny)/vertical_inc)+1
        if f_maxv > maxv:
            fmaxv = maxv
        print "\n______\nfacet: {0}\nf_minh: {1},f_maxh: {2}, f_minv: {3}, f_maxv: {4}\n".format(facet.string(),f_minh,f_maxh,f_minv,f_maxv)
        for h in range(f_minh,f_maxh):
            for v in range(f_minv,f_maxv):
                x = cvt_pix_2_coord(h,minx,horizontal_inc)#float(h)*horizontal_inc+minx
                y = cvt_pix_2_coord(v,miny,vertical_inc)# y = float(v)*vertical_inc+miny
                if not facet.ray_in_poly(x,y):
                    z = minz
                else:
                    z = facet.calcZ(x,y)
                if z > heights[h][v]:
                    heights[h][v] = int(256*(z - minz)/z_range)
                print "\th,v: {0},{1}; x,y: {2},{3}; z: {4}".format(h,v,x,y,z)
    print "Done: {} seconds".format( round(time.clock(),3))
    outdata = open(outdataname,"wt")
    hwide = len(heights)
    hhigh = len(heights[0])
    
    print "hwide: {}; hhigh: {}".format(hwide,hhigh)
    minh = 2e200
    maxh = -2e200
    
    for h in range(0,len(heights)):
    	for v in range(0,len(heights[h])):
    		if heights[h][v] > maxh:
    			maxh = heights[h][v]
    		if heights[h][v] < minh: 
    			minh = heights[h][v]
    r = abs(minh-maxh)			
    print "Minh,maxh: {0},{1}; range: {2}".format(minh, maxh,r)
    for h in range(0,len(heights)):
    	for v in range(0,len(heights[h])):
    		z = heights[h][v]
    		gray = ((z-minh)/r)
    		outdata.write("%3d" % gray)
    		outdata.write(" ")
    	outdata.write("\n")
    print "heights",heights
    outdata.close()
    print "Exit: {} seconds".format( round(time.clock(),3))
 
        

if __name__ == "__main__":
    main()    
