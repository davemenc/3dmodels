import random
class Point:
    """ Point class represents and manipulates x,y,z coords. """

    def __init__(self,x=None,y=None,z=None,pt_size=100):
        """ Create a new point at the origin """
        if x is None:
            self.x = (1 if random.randrange(1)==0 else -1) * pt_size*float(random.randrange(1000))/1000.0
        else:
            self.x = float(x)
        if y is None:
            self.y = (1 if random.randrange(1)==0 else -1) * pt_size*float(random.randrange(1000))/1000.0
        else:
            self.y = float(y)
        if z is None:
            self.z = (1 if random.randrange(1)==0 else -1) * pt_size*float(random.randrange(1000))/1000.0
        else:
            self.z = float(z)
           
    def string(self):
            return "Point: {0}, {1}, {2}".format(self.x,self.y,self.z)
	

class Poly:
    """ Polygon class represents and manipulates 3 sided polygons."""

    def __init__(self,p1=None,p2=None,p3=None,poly_size=100):
            """ create a new poly consiting of 3 points"""
            if p1 is None:
                self.p1 = Point(pt_size=poly_size)
            else:
                self.p1 = p1

            if p2 is None:
                self.p2 = Point(pt_size=poly_size)
            else:
                self.p2 = p2

            if p3 is None:
                self.p3 = Point(pt_size=poly_size)
            else:
               self.p3 = p3
            self.max_x = None
            self.max_y = None
            self.max_z = None
            self.min_x = None
            self.min_y = None
            self.min_z = None

    def  random_point(self,pt_size):
            """ create a random polygon  with all values between -size to size """
            

    def string(self):
            return "Poly: {0}; {1}; {2}".format(self.p1.string(), self.p2.string(), self.p3.string())

    def ray_in_poly(self,pt):
            """ pt is a vertical ray; does it pass through the poly?"""
            """ note that a vertical ray has no z so we ignore z value of pt"""
            if self.max_x is None: #if we haven't calculated the min and max values for this poly do it now
                self.max_x = max(self.p1.x,self.p2.x,self.p3.x)
                self.max_y = max(self.p1.y,self.p2.y,self.p3.y)
                self.max_z = max(self.p1.z,self.p2.z,self.p3.z)
                self.min_x = min(self.p1.x,self.p2.x,self.p3.x)
                self.min_y = min(self.p1.y,self.p2.y,self.p3.y)
                self.min_z = min(self.p1.z,self.p2.z,self.p3.z)

            #this is a bounding box test -- if it fails we KNOW point isn't in polygon
            if pt.x<self.min_x or pt.x>self.max_x or pt.y<self.min_y or pt.y>self.max_y:
                return False
            #for now let's stop here
            return True
    def point_height(self,pt):
        """we have a point in the poly: what is the z value?"""
        if self.p1.z == self.p2.z:
            z1 = self.p1.z
            z4 = self.p1.z
        else: 
            z1 = pt.x*abs(self.p1.x-self.p2.x)/abs(self.p1.z-self.p2.z)
            z4 = pt.y*abs(self.p1.y-self.p2.y)/abs(self.p1.z-self.p2.z)
        if self.p1.z == self.p3.z:
            z2 = self.p1.z
            z5 = self.p1.z
        else:
            z2 = pt.x*abs(self.p1.x-self.p3.x)/abs(self.p1.z-self.p3.z)
            z5 = pt.y*abs(self.p1.y-self.p3.y)/abs(self.p1.z-self.p3.z)
        if self.p2.z == self.p3.z:
            z3 = self.p3.z
            z6 = self.p3.z
        else:
            z3 = pt.x*abs(self.p2.x-self.p3.x)/abs(self.p2.z-self.p3.z)
            z6 = pt.y*abs(self.p2.y-self.p3.y)/abs(self.p2.z-self.p3.z)
        z = (z1+z2+z3+z4+z5+z6)/6
        return z
	def cross(self,poly):
		c = 
def main():

    poly = Poly(Point(0,0,0),Point(0,10,10),Point(10,0,0))
    ray = Point(3.33,3.33,0)
    print "poly",poly.string()
    print "ray",ray.string()
    print poly.ray_in_poly(ray)
    print "height",poly.point_height(ray)
    exit()
    SIZE = 10 #the max value of 
    POLY_COUNT = 1 #total number of polys generated
    HORIZONTAL_RAYS = 100
    VERTICAL_RAYS = HORIZONTAL_RAYS
    poly_list = []
    for i in range(0,POLY_COUNT):
        poly_list.append(Poly(poly_size=SIZE))

    for x_ray in range(0,HORIZONTAL_RAYS):
        for y_ray in range(0,VERTICAL_RAYS):
            x = x_ray*(float(SIZE)*2/float(HORIZONTAL_RAYS))-SIZE
            y = y_ray*(float(SIZE)*2/float(VERTICAL_RAYS))-SIZE
            ray = Point(x,y,0)
            #print "{0},{1}: {2},{3} -> {4}".format(x_ray,y_ray,x,y,ray.string())
            #print "RAY:",ray.string()
            for poly in poly_list:
                #print "POLY:",poly.string()
                inpoly = poly.ray_in_poly(ray)
                if inpoly:
                    print "ray {0} is in {1}\n".format(ray.string(),poly.string())

if __name__ == "__main__":
	main()
