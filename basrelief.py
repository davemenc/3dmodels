'this file reads a 3d stl file and generates a 2d grayscale image where the highest points are lighter than the lowest points'
import random
import time
from vector import Vector
from polygon import Polygon
from solid import Solid
from PIL import Image

def cvt_pix_2_coord(pix, min_coord, inc):
    """#need a way to convert a h,v into an x,y  # x = float(h)*horizontal_inc+minx"""
    return float(pix) * inc + min_coord

def cvt_coord_2_pix(coord, min_coord, inc, limit):
    """ # need a way to convert a raw x,y to a pix# in h and v  # h = int((x-minx)/horizontal_inc) """
    result =  int((coord - min_coord)/inc)
    if result > limit: result = limit
    if result < 0: result = 0
    return result


        
def main():
    ######### DEFINE CONSTANTS #########
    #inname = "dave_processed.stl"
    #inname = "cube.stl"
    #inname = "asymetric_rotated_cube.stl"
    #inname = "rotated_dave.stl"
    inname = "rotated_cube.stl"
    print "1 *** Start: {} at seconds (file: {})".format( round(time.clock(),3),inname)
    outimage = inname+".png"
    MM_PER_INCH = 25.4
    HORIZ_RESOLUTION_DPI = 300 
    VERT_RESOLUTION_DPI = 300 
    horiz_resolution_dpmm = int(HORIZ_RESOLUTION_DPI/MM_PER_INCH)
    vert_resolution_dpmm = int(VERT_RESOLUTION_DPI/MM_PER_INCH) 

    outfile = "testdata.txt"
    outdata = open(outfile,"wt")
    
    ########### READ STL FILE ########
    solid = Solid()
    solid.read_stl_file(inname)
    print "2 *** File {1} parse complete at {0} seconds".format( round(time.clock(),3),inname)
    
    ########### FILE HAS BEEN READ ########
    maxx = solid.min_max['maxx']
    minx = solid.min_max['minx']
    maxy = solid.min_max['maxy']
    miny = solid.min_max['miny']
    minz = solid.min_max['minz']
    maxz = solid.min_max['maxz']
    depth = abs(minz-maxz) 
    width = abs(maxx-minx) 
    height = abs(maxy-miny)
    #NB: all these values can be considered mm; that's what I assume
    hpix = int(horiz_resolution_dpmm * width+0.999)
    vpix = int(vert_resolution_dpmm * height+0.999)
    delta_x = width / hpix
    delta_y = height / vpix 

    heights = [[minz for y in xrange(vpix)] for x in xrange(hpix)]
    #print "3 *** Heights array allocated at {0} seconds".format( round(time.clock(),3))
    
    """ 
        The algorithm is to take each poly and look at it from above. 
        Create a grid of pixels over it (all the pixels in a bounding box). 
        For each pixel, determine if it's in the polygon.
        If it IS, find out the height where it crosses the polygon and record it.
        Since the same pixel may cross many polygons, record the HIGHEST point. 
        
        
    """
    for facet in solid.facet_list: # let's examine one facet/polygone/face
        #Now we create a pixel bounding box
        # delta_x and delta_y are the dist between pixels in model coordinates (perhaps mm).\
        min_pix_h = cvt_coord_2_pix(facet.min_x,minx,delta_x,hpix)-1
        if min_pix_h<0: min_pix_h=0
        max_pix_h = cvt_coord_2_pix(facet.max_x,minx,delta_x,hpix)+1
        if max_pix_h > hpix: max_pix_h = hpix
        min_pix_v = cvt_coord_2_pix(facet.min_y,miny,delta_y,vpix)-1
        if min_pix_v < 0: min_pix_v=0
        max_pix_v = cvt_coord_2_pix(facet.max_y,miny,delta_y,vpix)+1
        if max_pix_v>vpix: max_pix_v=vpix

        # now we step through all the pixels in that bounding box
        for h in range(min_pix_h,max_pix_h):
            for v in range(min_pix_v,max_pix_v):
                x = cvt_pix_2_coord(h,minx,delta_x)
                y = cvt_pix_2_coord(v,miny,delta_y)
                """ The 4 pixels we know are wrong are 99,68, 99,69, 99,70 and 99, 168"""               
                if not facet.ray_in_poly(x,y):
                    z = minz
                else:
                    z = facet.calcZ(x,y)
                if z > heights[h][v]:
                    heights[h][v] = z
    print "4 *** Facets Done at: {} seconds".format( round(time.clock(),3))
    hwide = len(heights)
    hhigh = len(heights[0])
    
    min_depth = 2e200
    max_depth = -2e200
    for h in range(0,hpix):
        for v in range(0,vpix):
            outdata.write("%5.1f" % heights[h][v])
            outdata.write(" ")
            if heights[h][v] > max_depth:
                max_depth = heights[h][v]
            if heights[h][v] < min_depth: 
                min_depth = heights[h][v]
        outdata.write("\n")
    print "5 *** Minz/Maxz Done at: {} seconds".format( round(time.clock(),3))
    
    r = abs(max_depth-min_depth)          
    
    pix = []
    for v in range(vpix-1,-1,-1):
        for h in range(0,hpix):
            z = heights[h][v]
            gray = int(255*(float(z-min_depth)/float(r))/2)
            pix.append((gray,gray,gray))

    outdata.close()
    print "6 *** File Closed at: {} seconds".format( round(time.clock(),3))
    im2 = Image.new("RGB",(hpix,vpix))
    im2.putdata(pix)
    im2.save(outimage,"PNG")
    #im2.show()
    print "7 *** Exit: at {} seconds".format( round(time.clock(),3))
    
    
    
if __name__ == "__main__":
    main()    
