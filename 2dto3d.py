#import Mesh

def add_face():
	pass
def pix_edge_face(pix):
	pass
def pix_corner_face(pix):
	pass
def pix_corner_face(pix):
	pass
def vert_rect_face(x1,y1,z1,x2,y2,z2):
	""" 
		Vertical rectilinear face; defined by vertical lines at x1,y1 and x2,y2 and horizantal right angle lines at z1 and z2
		the area is the plane defined by those two vertical lines, constrained by the two horizontal lines at z1 and z1.
		Since faces are always triangles, we bisect that rectanglular plane and return 2 faces 
		Note that if you flip the ordering you can flip the normals on the faces.
	"""
	face1 = [[x1,y1,z1],[x1,y1,z2],[x2,y2,z2]]
	face2 = [[x2,y2,z2],[x2,y2,z1],[x1,y1,z1]]
	return [face1,face2]
def horiz_rect_face(x1,y1,x2,y2,z):
	"""
		Horizontal rectilinear face; 
	"""
	face1 = [[x1,y1,z],[x2,y1,z],[x1,y2,z]]
	face2 = [[x2,y1,z],[x2,y2,z],[x1,y2,z]]
	return [face1,face2]

def read_2d_data(fname):
	width = -1
	height = -1
	min_z = 10000000
	max_z = -1000000
	pix = {}
	y = -1
	x = -1
	with open(fname,"rt") as f:
		for line in f:
			if len(line)<10: #skip blank lines
				continue
			data = line.strip().split("\t")
			if width < 0:
				width = len(data)
			line_len = len(line)
			if line_len < width: # exend data to width
				for i in range(0,width-line_len):
					data.append(0)
			y += 1
			for x in range(0,width):
				pix[(x,y)] = float(data[x])
				if pix[(x,y)]>max_z:
					max_z = pix[(x,y)] 
				if pix[(x,y)]<min_z:
					min_z = pix[(x,y)] 
		height = y+1 #max y
		depth = max_z - min_z +1
		pix['depth'] = depth
		pix['width'] = width
		pix['height'] = height
		pix['max_z'] = max_z
		pix['min_z'] = min_z
	return pix
def fetch_pix(x,y,data):
	idx = (x,y)
	if idx in data:
		return float(data[idx])
	else:
		return 0.0
def test_box():
	width = 100
	height = 100
	depth = 100
	x0=0
	y0=0
	z0=0
	top  = horiz_rect_face(x0,y0,width,height,depth)  
	bottom = horiz_rect_face(width,y0,x0,height,z0)
	front = vert_rect_face(x0,y0,depth,width,y0,z0)
	left = vert_rect_face(x0,y0,z0,x0,height,depth)
	right = vert_rect_face(width,y0,depth,width,height,z0)
	back = vert_rect_face(x0,height,z0,width,height,depth)
	#what = vert_rect_face(width,y0,z0,width,height,depth)

	faces = top+bottom+front+back+left+right
	print("\n#______________________________")
	print('import Mesh')
	print('App.getDocument("Unnamed").removeObject("Mesh")')
	print(f"faces = {faces}")
	print("meshObject = Mesh.Mesh(faces)")
	print("Mesh.show(meshObject)")
	print('Gui.SendMsgToActiveView("ViewFit")')
	print("#__________________________________")
	print()
	print()
def display_freecad_mesh(faces):
	print("\n#______________________________")
	print('import Mesh')
	print('App.getDocument("Unnamed").removeObject("Mesh")')
	print(f"faces = {faces}")
	print("meshObject = Mesh.Mesh(faces)")
	print("Mesh.show(meshObject)")
	print('Gui.SendMsgToActiveView("ViewFit")')
	print("#__________________________________")
	print()
	print()

if __name__ == "__main__":
	#fname = "D:/DEV/3D/images/square_grey_pix.tsv"
	fname = "images/tiki100.tsv"
	data = read_2d_data(fname)
	depth = data['depth']
	width = data['width']
	height = data['height'] 
	max_z = data['max_z']
	min_z = data['min_z']
	mesh = []

	x0=0
	y0=0
	z0=0
	#bottomleftcorner
	x=0
	y=0
	z=fetch_pix(x,y,data)
	x1=1
	y1=1
	z1 = fetch_pix(x1,y1,data)

	print(z,z1)
	exit()
	#bottom
	mesh+=horiz_rect_face(width-1,y0,x0,height-1,z0)

	# front
	y = 0	
	for x in range(0,width-1):
		z = fetch_pix(x,y,data)
		x1 = x+1
		face1 = [[[x,y,z],[x,y,z0],[x1,y,z0]]]
		face2 = [[[x,y,z],[x1,y,z0],[x1,y,z]]]
		mesh += face1+face2

	# rear
	y = height-1
	for x in range(0,width-1):
		z = fetch_pix(x,y,data)
		x1 = x+1
		face1=[[[x,y,z],[x1,y,z0],[x,y,z0]]]
		face2=[[[x,y,z],[x1,y,z],[x1,y,z0]]]
		mesh += face1+face2

	#left
	x = 0
	for y in range(0,height-1):
		z = fetch_pix(x,y,data)
		y1 = y+1
		face1 = [[[x,y1,z],[x,y1,z0],[x,y0,z0]]]
		face2 = [[[x,y0,z0],[x,y0,z],[x,y,z]]]
		mesh += face1+face2
	#right
	x = width-1
	for y in range(0,height-1):
		z = fetch_pix(x,y,data)
		y1 = y+1
		face1 = [[[x,y1,z],[x,y0,z0],[x,y1,z0]]]
		face2 = [[[x,y0,z0],[x,y,z],[x,y0,z]]]
		mesh += face1+face2
	#top
	for x in range(1,width-1):
		for y in range(1,height-1):
			z = fetch_pix(x,y,data)
			x1 = x-.5
			y1 = y+.5
			z1 = (z+fetch_pix(x-1,y+1,data))/2.0

			x2 = x+.5
			y2 = y+.5
			z2 = (z+fetch_pix(x+1,y+1,data))/2.0

			x3 = x+.5
			y3 = y-.5
			z3 = (z+fetch_pix(x+1,y-1,data))/2.0
			
			x4 = x-.5
			y4 = y-.5
			z4 = (z+fetch_pix(x-1,y-1,data))/2.0
			
			face1 = [[[x,y,z],[x2,y2,z2],[x1,y1,z1]]]
			face2 = [[[x,y,z],[x3,y3,z3],[x2,y2,z2]]]
			face3 = [[[x,y,z],[x4,y4,z4],[x3,y3,z3]]]
			face4 = [[[x,y,z],[x1,y1,z1],[x4,y4,z4]]]
			mesh+=face1+face2+face3+face4
	display_freecad_mesh(mesh)
	print(f"#faces={len(mesh)}")