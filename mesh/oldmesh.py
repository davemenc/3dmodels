#from face import Face
#from vertex import Vertex
#from edge import Edge
class Mesh:
	def __init__(self,fname):
		self.fname = fname
		self.faces = {}
		self.edges = {}
		self.vertices = {}
	def load_stl_text(self):
		"""
		solid newdave2
			facet normal -0.796945 -0.412547 0.441230
				outer loop
					vertex -68.026917 356.809021 -480.433838
					vertex -68.026917 356.167755 -481.033417
					vertex -66.581787 354.017395 -480.433807
				endloop
			endfacet
		endsolid
 		"""
		line = 0
		with open(self.fname,"rt") as f:
			for line in f:
				line += 1
				parts = line.strip().split(" ")
				partlen = len(parts)
				currentnormal = None
				v_q = [] # this is the last 3 vertexes we processed
				if partlen == 0:
					continue
				type = parts[0]

				if type=="vertex":
					if partlen < 4:
						print(f"Bad vertex: not enough numbers in  {line} parsed as {parts}; line {line}")
						continue
					v = Vertex(parts[1],parts[2],parts[3])
					idx = (parts[1],parts[2],part[3])
					if idx in self.vertices:
						self.vertices[idx].append(v)
					else:
						self.vertices[idx] = [v]
					v_q.append(v)
				elif type == "facet":
					if partlen < 5:
						print(f"Bad facet: not enough parts in {line} parsed as {parts}; line {line}")
						continue
					currentnormal = Vertex(parts[2],parts[3],parts[4])
				elif type == "endfacet":
					currentnormal = None
					if len(v_q) > 0:
						print(f"Error in endfacet: vertices left in que: {v_q}; line {line}")
						v_q = []
				elif type == "outer":
					pass
				elif type == "endloop":
					if len(v_q) != 3:
						print(f"Error in endloop: Not enough vertexes left in que: {v_q}; line {line}")
					e1 = Edge(v_q[0],v_q[1])
					idx1 = fucket #giving up; I don't know how to index the various facets, edges and so forth; faces aren't so bad but edges have two ways of being index
					e2 = Edge(v_q[1],v_q[2])
					e3 = Edge(v_q[2],v_q[0])

				elif type == "endsolid":
					print("\n___________________________________________________________\n")
					print(f"{line} lines processed")
					print(f"vertices: {len(self.vertices)}")
					print(f"edges: {len(self.edges)}")
					print(f"faces: {len(self.faces)}")
					print("\n___________________________________________________________\n")		
				else:
					print(f"line {line} with parts {parts} has a bad command {parts[0]}; line {line}")
if __name__ == "__main__":
	fname = "cubecube.stl"
	m = Mesh(fname)