fname = "listofcommands.txt"
counts = {}
with open(fname,"rt") as f:
	for line in f:
		line = line.strip().split(" ")[0]
		if line in counts:
			counts[line] += 1
		else:
			counts[line] = 1
for command in counts.keys():
	print(f"{command} has {counts[command]} instances.")