from vector import Vector
from polygon import Polygon

class Solid:
    """ A collection of facets (polygons) that presumably make up a solid """
    def __init__(self,name="",normal=None):
        self.min_max = {'minx':2e308, 'maxx':-2e308,'miny':2e308, 'maxy':-2e308,'minz':2e308, 'maxz':-2e308,} 
        self.facet_list = []
        self.name = name
        self.face_count = 0
        
    def add_facet(self,facet):
        facet.calculate()
        self.facet_list.append(facet)
        self.face_count += 1
        self.min_max['minx'] = min(self.min_max['minx'],facet.min_x)
        self.min_max['miny'] = min(self.min_max['miny'],facet.min_y)
        self.min_max['minz'] = min(self.min_max['minz'],facet.min_z)
        self.min_max['maxx'] = max(self.min_max['maxx'],facet.max_x)
        self.min_max['maxy'] = max(self.min_max['maxy'],facet.max_y)
        self.min_max['maxz'] = max(self.min_max['maxz'],facet.max_z)

    def _tokenize(self, s):
        s = s.strip()
        stoks = s.split(" ")
        ttoks = []
        for t in range(0,len(stoks)):
            stoks[t] = stoks[t].strip()
            if len(stoks[t])>0:
                ttoks.extend(stoks[t].split("\t"))
        rtoks = []
        for t in range(0,len(ttoks)):
            ttoks[t]=ttoks[t].strip()
            if len(ttoks[t])>0:
                rtoks.append(ttoks[t])
        return rtoks

    def _concat_list(self, l):
        r=""
        for s in l:
           r += s+" "
        return r

    def _str_to_float(self, s):
        return float(s)

    def _parseline(self, line,state):
        #print line,state
        # check for blank line
        if len(line)<3:
            return state

        # self._tokenize the line
        toks = self._tokenize(line)
        #if we don't have the name of the solid, look for it
        if not state['solid']:
            if toks[0].lower()=='solid':
                del(toks[0])
                state['sname']=self._concat_list(toks)
                state['solid'] = True
                self.name = state['sname']
                return state
            else:
                return state

        #if we don't have the facet keyword, look for it
        if not state['facet']:
            if toks[0].lower()== 'facet':
                state['facet'] = True
                if toks[1].lower() == 'normal': # we have normal info
                    normal = Vector(self._str_to_float(toks[2]),self._str_to_float(toks[3]),self._str_to_float(toks[4]))
                    state['poly']=Polygon(None,None,None,normal)

                return state
            else:
                return state

        #if we don't have the outer loop  keywords, look for it
        if not state['outer']:
            if toks[0].lower()=='outer':
                state['outer'] = True
            else:
                return state

        #if we don't have the vertexes
        if state['vertex'] < 3:
            if toks[0].lower() == 'vertex':
                state['poly'].add_vertex(Vector(self._str_to_float(toks[1]),self._str_to_float(toks[2]),self._str_to_float(toks[3])))
                state['vertex'] += 1
                return state
            else:
                return state

        #if we have the endloop, consume it
        if toks[0] == 'endloop':
            return state
        #if we have endfacet, we're done with this facet
        if toks[0] == 'endfacet':
            state['facet'] = False
            state['outer'] = False
            state['vertex'] = 0
            state['poly'].calculate()
            self.add_facet(state['poly'])
        return state

    def read_stl_file(self, inname):
        state = {'solid':False,'facet':False,'outer':False,'vertex':0,'sname':""}
        with open(inname, 'rt') as f:
            for line in f:
                state = self._parseline(line,state)
        return state

    def write_stl_vertex(self,p,out):
        out.write("   vertex {0} {1} {2}\n".format(p.x,p.y,p.z))

    def write_stl_facet(self,f,out):
        if f.normal != None:
            out.write(" facet normal {0} {1} {2}\n".format(f.normal.x,f.normal.y,f.normal.z))
        out.write("  outer loop\n")
        self.write_stl_vertex(f.p1,out)
        self.write_stl_vertex(f.p2,out)
        self.write_stl_vertex(f.p3,out)
        out.write("  endloop\n")
        out.write(" endfacet\n")

    def write_stl_file(self,outfname,modelname=""):
        out = open(outfname,"w")
        out.write("solid cube - {}\n".format(modelname))
        for f in self.facet_list:
            self.write_stl_facet(f,out)
        out.write("endsolid cube - {}\n".format(modelname))
        out.close()
            
    def string(self):
        s = "Solid: {0} \n".format(self.name)
        for f in self.facet_list:
            s += f.string()+"\n"
        s += "Faces: {}\n".format(self.face_count)
        s += "\t\tX\t\tY\t\tZ\n"
        s += "Min:\t\t{0}\t{1}\t{2}\n".format(self.min_max['minx'],self.min_max['miny'],self.min_max['minz'])
        s += "Max:\t\t{0}\t{1}\t{2}\n".format(self.min_max['maxx'],self.min_max['maxy'],self.min_max['maxz'])
        s += "Dim:\t\t{0}\t{1}\t{2}\n".format(abs(self.min_max['minx']-self.min_max['maxx']),
                                              abs(self.min_max['miny']-self.min_max['maxy']),
                                              abs(self.min_max['minz']-self.min_max['maxz']))
        
        return s
