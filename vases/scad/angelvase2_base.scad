$fn=200;
base_width=150;
base_thick = 8;
difference()
{
	union(){
		translate([0,0,300-1])rotate([180,0,0])import("D:/Making/3D/scad/Angel/angelvase2_done.stl");
		cylinder(h=base_thick, d=base_width);
		rotate_extrude(convexity = 10)
		translate([base_width/2, 0, 0])
		circle(d = base_thick*2);
	}
	translate([0,0,-base_thick])cylinder(h=base_thick,d=base_width+base_thick*2+2);
}
import("D:/Making/3D/scad/Angel/Wings2.stl");
