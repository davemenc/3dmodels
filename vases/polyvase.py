from pixels import Pixels 
def write_scadfile(fname,data):
	with open(fname,"wt") as f:
		f.write("rotate_extrude(angle=360,convexity=20,$fn=200)\npolygon([[0.0, 0],")
		for i in range(0,len(data)):
			f.write(f"[{str(data[i])},{str(i)}],")
		f.write(f"[0.0, {str(i)}]]);")

extender=".png"
path_name = "D:/!A_New_Downloads/"
path_name = "D:/DEV/3D/images/"

path_name = "D:/Making/3D/scad/Angel/"
vase_name = "angelvase2_600"

path_name = "D:/DEV/3D/images/"
vase_name = "snifter2"

path_name = "D:/DEV/3D/vases/"
vase_name = "classic"
vase_name = "doublegourd"
vase_name = "simplevase"
vase_name = "random"
vase_name = "tree.png"
vase_name = "roundpot_bw"
vase_name = "roundpot3"
vase_name = "chinesevase_bw"



img_name =  path_name+vase_name+extender
print(f"Image file: {img_name}")

p = Pixels(img_name)
print(f"from {p.fname} in mode {p.mode} we get {p.width}X{p.height}; maxval:{p.maxval}; minval: {p.minval} ")
widths = []
for y in range(p.height):
	first = -1
	last = -1
	for x in range(p.width):
		pix = p.fetch_pixel(x,y)
		if pix == 0:
			if first==-1:
				first = x
			if x>last:
				last=x
		if first>-1 and last>-1 and pix>0:
			break
	width = last-first+1
	#print(first,last,width)
	if first<0 or last<0:
		continue
	if width < 10 :
		#widths.append(10)
		print(f"Row {y} is too narrow ({width}).")
		print(first,last,width)
		continue
	else:
		widths.append(width/2)
if len(widths)>0:
	print ("we have widths")
	print("total values: ",len(widths))
else:
	print ("*** ERROR: WE HAVE NO WIDTHS!")
	exit()
#print(widths)
#for w in widths:
#	print(w)
smooth=[-1]*len(widths)
smooth[0]=(widths[0]+widths[1])/2
smooth[1]=(widths[0]+widths[1]+widths[2])/3
smooth[-1]=(widths[-2]+widths[-1])/2
smooth[-2]=(widths[-1]+widths[-2]+widths[-3])/3

for i in range(2,len(widths)-2):
	smooth[i]=int((widths[i-2]+widths[i-1]+widths[i]+widths[i+1]+widths[i+2])/5)
print("we have smooth values")
print("------------------")
#print(len(widths),"==",len(smooth))
if len(widths)!=len(smooth):
	print(f"Lengths of widths and smooth values are not equal: widths: {len(widths)}, smooth: {len(smooth)}")
total_s = 0
count_s = 0
max_s = -1
min_s = 10000
total_w = 0
count_w = 0
max_w = -1
min_w = 10000

for i in range(0,len(widths)):
	s = smooth[i]
	w = widths[i]
	#print(f"{i}: {w} -> {round(s,2)}; {round(s-w,2)}")
	total_s +=s
	total_w +=w
	if w>max_w:
		max_w = w 
	if s>max_s:
		max_s = s
	if w<min_w:
		min_w = w 
	if s<min_s:
		min_s = s 
	count_w += 1
	count_s +=1
#print (count_s,"==",count_w,"==",len(widths),"==",len(smooth))
print("totals: ",total_w,round(total_s,2),round(total_w-total_s,2),round((total_w-total_s)/count_s,2))
print("averages: ",round(total_w/count_w,2),round(total_s/count_s,2) )
print("------------------")
"""
old =widths[0]
for i in range(100,len(widths)):
	if old>widths[i]:
		print(i,old,widths[i])
	old = widths[i]
"""
openscadfile = vase_name+"_pg.scad"
write_scadfile(openscadfile,widths)
openscadfile_s =  vase_name+"_pg_s.scad"
write_scadfile(openscadfile_s,smooth)

"""
with open(openscadfile,"wt") as f:
	first = True
	f.write("rotate_extrude(angle=360,convexity=20,$fn=31)\npolygon([[0.0, 0],")
	for i in range(0,len(widths)):
		f.write(f"[{str(width)},{str(i)}],")
	f.write("[0.0, 0]];")
"""
"""
with open(openscadfile_s,"wt") as f:
	first = True
	f.write(f"widths = [")
	for s in smooth:
		if not first:
			f.write(",")
		else:
			first=False
		f.write(str(s))
	f.write("];")
"""
print("saved to ",openscadfile_s,"and", openscadfile)