fname_in = "D:/DEV/3D/vases/snifter2_s.scad"
fname_out = "D:/making/3d/scad/vases/pointlist.scad"
print("In:",fname_in)

print("Out:",fname_out)
points = []
with open(fname_in,"rt") as f:
	s = f.read().strip()
w=s.find("widths = [")
s=s[w+10:]
eb = s.find("];")
s = s[0:eb]
points = s.split(",")
smallest=100000.0
data =[]
for d in points:
	if float(d)<smallest:
		smallest=float(d)
	data.append(float(d))

out_s = "point_list=["
last = f"[{data[0]},0],"
print("Count:",len(data))
for i in range(0,len(data)):
	out_s += f"[{data[i]-smallest},{i}],"
out_s+=last
out_s=out_s[0:-1]
out_s += "];"
out_s+=f"\npoint_count={len(data)};\n"
with open(fname_out,"wt") as f:
	f.write(out_s)