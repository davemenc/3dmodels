# Making a Vase
## Method 3
1. create a 2d image of the vase from the side
	1. I used pictures I got off the internet with a lot of processing but you could just use a curve
	2. the image needs to be black and white and have both sides of the vase; black for vase, white for background
	3. I found the best way to do it is to a perfectly black vector image (be sure to black the edges of inner objects if it's not just one outline. and then just export it
2. save that as a png, preferably black and white
	1. go over the outline with the black pen
	2. fill & pencil over the interior of your black outline in black
	3. Select the black body with the wand, invert the selection, paint the background in white
	4. Save it as a PNG
	5. Actualy the program looks for the first non-white pixel from the right and from the left
3. Edit polyvase3.py and put the path and the filename (without the .png) in the program
	1. you can also set the wall thickness
	2. you can also turn on and off the smoothing; it's recommended to turn it on 
4. Run the program. It will generate a scad file (and tell you the name); this will be a hollow shape
5. Load this into openscad and do an f6 followed by and f7
5. This is, more or less, ready to print, but probably upside down. 

## Method 1 (deprecated)
1. create a 2d image of the vase from the side
	1. I used pictures I got off the internet with a lot of processing but you could just use a curve
	2. the image needs to be black and white and have both sides of the vase; black for vase, white for background
	3. I found the best way to do it is to a perfectly black vector image (be sure to black the edges of inner objects if it's not just one outline. and then just export it
2. save that as a png, preferably black and white
3. I had to open it up in gimp because it had made it grayscale; I saved it in a 2 color pallette
	1. the program has SOME flexibility so gray scale might work if the vase is all 0s 
4. run it through "stackedcyl.py" by putting the name of the png in the program with the extender
	1. the python program will read from %name>.png and write to %name>.scad
	2. there might be some steps that will smooth or correct the data (the s version is smoothed).
5. open "datavase.scad"
6. include %name.scad in database and run it
7. it will take a while to render but it will ultimately (if the image isn't TOO high -- it's only the y that's the problem. complete -- 20-40 minutes is what I've seen for 700 pix heights
8. save the stl file
9. Open in meshlab
	1. filters->smoothing...->Laplace smooth improves it marginally
	2. filters->smoothing..->taubin lbmbda=.7, mu = -0.7
	3. filters->remeshing, simplication...->simplification quadratic edge...; reduce faces to 10,000
	4. save mesh as %original +m; specify stl (defualt is ply)
10. open in netfab
	1. cut the top and bottom so they are where you want them.
	2. rotate it so the bottom is up
	3. scale it to some reasonable size
	4. center it so the top (remember it's upside down so top of vase=bottom of model now) is at z=0 and the center is at x=0,y=0 (center to origin but set the z to 0).
	5. save the fullsize model as xxx_outer
	6. scale the model to be 6mm less in x & y BUT exactly the same in z (this gives a 3mm wall.
		1. may want to do more than 3mm wall
		2. in some cases may want to scale in z 1-4mm
	7. cut off the top 6-10 mm (this forms the floor of the vase. (could be as little as 3.
	8. recenter it to the origin, still with the top on the x=0,y=0 
	9. save it as "xxx_inner"
11. open a scad file with the difference(). of outer and inner 
	1. you can add a base (__be sure to overlap__)
	2. you can cut off the top and bottom
	3. may be better to just hollow it out
	4. sometimes you want to cut out the mouth
	5. once I added wings
	6. if you're running into trouble, simplify.
	7. render this 
	8. save this as "xxx_done"
	9. should have a hollow vase shape with a decent thickness
12. open in netfab again and, if necessary cut off the top 1mm to open it up completely.
13. possibly add something to the edge in open scad
14. possibly add somehting to the bottom in open scad
15. maybe add addornments once the basic shape is done

## Horizantality
There is a problem with #11. If the vase is purely vertical this works PERFECTLY. But if the vase has curves this will work less well. To the degree an edge moves horizontally the wall will be thinner. If the wall is 45 degrees, it will have 1/2 the thickness of a normal wall. If the wall is horizontal, it will have zero thickness. 
Angled walls ("curviness". can be helped by thicker walls. The bottom and top can simply be cut off from the inner, or, possibly, the model can be shrunk in the z dimension as it is in the x-y dimensions. But it's not perfect. 