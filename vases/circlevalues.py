#this program generates the values for a half circle and puts them in openscad format
import math
ANGLES = 100
RADIUS = 10
OUTFILENAME = "circle_data.scad"
point_list = {}

for i in range(0,ANGLES):
	angle = i*180/ANGLES
	x = RADIUS*(math.cos(math.radians(angle))+1)
	y = RADIUS*(math.sin(math.radians(angle))+1)
	point_list[i]=(x,y)
x = RADIUS*(math.cos(math.radians(180))+1)
y = RADIUS*(math.sin(math.radians(180))+1)

with open(OUTFILENAME,"wt") as f:
	first = True
	f.write("point_list=[")
	for idx,vals in point_list.items():
		x=vals[0]
		y=vals[1]-10
		f.write(f"[{y},{x}],")
	f.write("[0,0]];\n")

