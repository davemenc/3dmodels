#gen_height_map.py
from pixels import Pixels 
extender=".png"
vase_name = "doublegourd"
vase_name ="random"
vase_name = "classic"
vase_name = "simple_vase_half3"
vase_name = "snifter2"
vase_name = "angelvase2_600"
vase_name = "angelvase2_600"
vase_name = "snowangel3_152"
path_name = "D:/!A_New_Downloads/"
path_name = "D:/DEV/3D/vases/"
path_name = "D:/DEV/3D/images/"
path_name = "D:/Making/3D/scad/Angel/"
path_name = "D:/Making/3D/scad/Angel/"
img_name =  path_name+vase_name+extender
print(img_name)
openscadfile = vase_name+".scad"
#img_name = "vase2.png"
#img_name = "vase1.png"
p = Pixels(img_name)
print(f"from {p.fname} in mode {p.mode} we get {p.width}X{p.height}; maxval:{p.maxval}; minval: {p.minval} ")
print(f"ouput file: {openscadfile}")
widths = []
for y in range(p.height):
	first = -1
	last = -1
	for x in range(p.width):
		pix = p.fetch_pixel(x,y)
		if pix == 0:
			if first==-1:
				first = x
			if x>last:
				last=x
		if first>-1 and last>-1 and pix>0:
			break
	width = last-first+1
	#print(first,last,width)
	if width < 10 :
		widths.append(10)
	else:
		widths.append(width)
print ("we have widths")
print("total values: ",len(widths))
print(widths)
#for w in widths:
#	print(w)
smooth=[-1]*len(widths)
smooth[0]=(widths[0]+widths[1])/2
smooth[1]=(widths[0]+widths[1]+widths[2])/3
smooth[-1]=(widths[-2]+widths[-1])/2
smooth[-2]=(widths[-1]+widths[-2]+widths[-3])/3

for i in range(2,len(widths)-2):
	smooth[i]=int((widths[i-2]+widths[i-1]+widths[i]+widths[i+1]+widths[i+2])/5)
print("we have smooth values")
print("------------------")
print(len(widths),"==",len(smooth))
total_s = 0
count_s = 0
max_s = -1
min_s = 10000
total_w = 0
count_w = 0
max_w = -1
min_w = 10000

for i in range(0,len(widths)):
	s = smooth[i]
	w = widths[i]
	print(f"{i}: {w} -> {round(s,2)}; {round(s-w,2)}")
	total_s +=s
	total_w +=w
	if w>max_w:
		max_w = w 
	if s>max_s:
		max_s = s
	if w<min_w:
		min_w = w 
	if s<min_s:
		min_s = s 
	count_w += 1
	count_s +=1
print (count_s,"==",count_w,"==",len(widths),"==",len(smooth))
print("totals: ",total_w,round(total_s,2),round(total_w-total_s,2),round((total_w-total_s)/count_s,2))
print("averages: ",round(total_w/count_w,2),round(total_s/count_s,2) )
print("------------------")
old =widths[0]
for i in range(100,len(widths)):
	if old>widths[i]:
		print(i,old,widths[i])
	old = widths[i]

with open(openscadfile,"wt") as f:
	first = True
	f.write(f"widths = [")
	for w in widths:
		if not first:
			f.write(",")
		else:
			first=False
		f.write(str(w))
	f.write("];")

openscadfile_s =  vase_name+"_s.scad"
with open(openscadfile_s,"wt") as f:
	first = True
	f.write(f"widths = [")
	for s in smooth:
		if not first:
			f.write(",")
		else:
			first=False
		f.write(str(s))
	f.write("];")
print("saved to ",openscadfile_s,"and", openscadfile)