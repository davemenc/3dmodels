from pixels import Pixels 
def write_scadfile(fname,data):
	with open(fname,"wt") as f:
		f.write("rotate_extrude(angle=360,convexity=20,$fn=200)\npolygon([[0.0, 0],")
		for i in range(0,len(data)):
			f.write(f"[{str(data[i])},{str(i)}],")
		f.write(f"[0.0, {str(i)}]]);")

extender=".png"
path_name = "D:/!A_New_Downloads/"
path_name = "D:/DEV/3D/images/"

path_name = "D:/Making/3D/scad/Angel/"
vase_name = "angelvase2_600"

path_name = "D:/DEV/3D/images/"
vase_name = "snifter2"

path_name = "D:/DEV/3D/vases/"
vase_name = "simplevase"
vase_name = "random"
vase_name = "tree.png"
vase_name = "roundpot_bw"
vase_name = "roundpot3"
vase_name = "chinesevase_bw"
vase_name = "classic"
vase_name = "doublegourd"


SMOOTHING = False
img_name =  path_name+vase_name+extender
print(f"Image file: {img_name}")

p = Pixels(img_name)
print(p.fname,p.mode)
print(f"from {p.fname} in mode {p.mode} we get {p.width}X{p.height}; maxval:{p.maxval}; minval: {p.minval} ")
white = p.maxval
widths = []
pixvals = {}
for y in range(p.height):
	first = -1
	last = -1
	for x in range(p.width):
		pix = p.fetch_pixel(x,y)
		#print(f"first: {y},{x}:{pix}")
		pixvals[(y,x)]=pix
		if pix != white: #first non-white pixel
			first = x
			break

	#print(f"transition1: {y},{x}: {pix}")

	for x in range (p.width-1,0,-1):
		pix =p.fetch_pixel(x,y)
		#print(f"last: {y},{x}:{pix}")
		if pix != white:
			last = x
			#print("last actual = ",p.im.getpixel((x,y)))
			break
	#print(f"transition2: {y},{x}: {pix}")
	width = last-first+1
	#print("PIX:  ",y,first,last,width)
	if first<0 or last<0:
		continue
	if width < 10 :
		#widths.append(10)
		#print(f"Row {y} is too narrow ({width}).")
		continue
	else:
		widths.append(width/2)
		#print("Width:",y,first,last,width)
#print("++++++++++++++++++++++++++++++")
#print(pixvals)
#print("++++++++++++++++++++++++++++++")
if len(widths)>0:
	print ("we have widths")
	print("total values: ",len(widths))
else:
	print ("*** ERROR: WE HAVE NO WIDTHS!")
	exit()

# SMOOTHING #	
if SMOOTHING:
	smooth=[-1]*len(widths)
	smooth[0]=(widths[0]+widths[1])/2
	smooth[1]=(widths[0]+widths[1]+widths[2])/3
	smooth[-1]=(widths[-2]+widths[-1])/2
	smooth[-2]=(widths[-1]+widths[-2]+widths[-3])/3

	for i in range(2,len(widths)-2):
		smooth[i]=int((widths[i-2]+widths[i-1]+widths[i]+widths[i+1]+widths[i+2])/5)
	print("we have smooth values")
	print("------------------")
	#print(len(widths),"==",len(smooth))
	if len(widths)!=len(smooth):
		print(f"Lengths of widths and smooth values are not equal: widths: {len(widths)}, smooth: {len(smooth)}")
	total_s = 0
	count_s = 0
	max_s = -1
	min_s = 10000
	total_w = 0
	count_w = 0
	max_w = -1
	min_w = 10000
if SMOOTHING:
	openscadfile_s =  vase_name+"_pg_s.scad"
	write_scadfile(openscadfile_s,smooth)
	print("saved to ", openscadfile_s)
else:
	openscadfile = vase_name+"_pg.scad"
	write_scadfile(openscadfile,widths)
	print("saved to ",openscadfile)

