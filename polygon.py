from vector import Vector
class Polygon:
    """ Polygon class represents and manipulates 3 sided polygons."""

    def __init__(self,p1=None,p2=None,p3=None,normal=None):
        self.normal = normal
        self.bound_xy = [] #should have 4 vects which are all on the same z plane and
        """ create a new facet consiting of 3 points"""
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

        self.max_x = None
        self.max_y = None
        self.max_z = None
        self.min_x = None
        self.min_y = None
        self.min_z = None

    def add_vertex(self,v):
        if self.p1 is None:
            self.p1 = v
        elif self.p2 is None:
            self.p2 = v
        elif self.p3 is None:
            self.p3 = v
        else:
            print "**** too many vertexes in a facet ****"

    def string(self):
        return "Poly: {0}; {1}; {2}".format(self.p1.string(), self.p2.string(), self.p3.string())

    def ray_in_poly(self,x,y):
        """ pt is a vertical ray; does it pass through the poly?"""
        """ note that a vertical ray has no z so we ignore z value of pt"""
        self.calculate()

        #this is a bounding box test -- if it fails we KNOW point isn't in polygon
        if x<self.min_x or x>self.max_x or y<self.min_y or y>self.max_y:
            return False
        denominator = ((self.p2.y - self.p3.y) * (self.p1.x - self.p3.x) + (self.p3.x - self.p2.x) * (self.p1.y - self.p3.y))
        if denominator == 0:
            denominator = .001
        a = ((self.p2.y - self.p3.y) * (x - self.p3.x) + (self.p3.x - self.p2.x) * (y - self.p3.y)) / denominator
        b = ((self.p3.y - self.p1.y) * (x - self.p3.x) + (self.p1.x - self.p3.x) * (y - self.p3.y)) / denominator 
        c = 1 - a - b
        return 0<=a and a<=1 and 0<=b and b<=1 and 0<=c and c<=1

    def point_height(self,x,y):
        """we have a point in the poly: what is the z value?"""
        if self.p1.z == self.p2.z:
            z1 = self.p1.z
            z4 = self.p1.z
        else: 
            z1 = x*abs(self.p1.x-self.p2.x)/abs(self.p1.z-self.p2.z)
            z4 = y*abs(self.p1.y-self.p2.y)/abs(self.p1.z-self.p2.z)
        if self.p1.z == self.p3.z:
            z2 = self.p1.z
            z5 = self.p1.z
        else:
            z2 = x*abs(self.p1.x-self.p3.x)/abs(self.p1.z-self.p3.z)
            z5 = y*abs(self.p1.y-self.p3.y)/abs(self.p1.z-self.p3.z)
        if self.p2.z == self.p3.z:
            z3 = self.p3.z
            z6 = self.p3.z
        else:
            z3 = x*abs(self.p2.x-self.p3.x)/abs(self.p2.z-self.p3.z)
            z6 = y*abs(self.p2.y-self.p3.y)/abs(self.p2.z-self.p3.z)
        z = (z1+z2+z3+z4+z5+z6)/6
        return z

    def calcZ(self, x, y):
        #print self.string(),x,y
        det = (self.p2.y - self.p3.y) * (self.p1.x - self.p3.x) + (self.p3.x - self.p2.x) * (self.p1.y - self.p3.y)
        if det == 0:
            det = 4.9e-324

        l1 = ((self.p2.y - self.p3.y) * (x - self.p3.x) + (self.p3.x - self.p2.x) * (y - self.p3.y)) / det
        l2 = ((self.p3.y - self.p1.y) * (x - self.p3.x) + (self.p1.x - self.p3.x) * (y - self.p3.y)) / det
        l3 = 1.0 - l1 - l2

        return l1 * self.p1.z + l2 * self.p2.z + l3 * self.p3.z


    def calculate(self):
        """ the polygon has some things like it's normal and it's bounding box that need to be calculated """
#        if self.max_x is None:
        self.max_x = max(self.p1.x,self.p2.x,self.p3.x)
        self.max_y = max(self.p1.y,self.p2.y,self.p3.y)
        self.max_z = max(self.p1.z,self.p2.z,self.p3.z)
        self.min_x = min(self.p1.x,self.p2.x,self.p3.x)
        self.min_y = min(self.p1.y,self.p2.y,self.p3.y)
        self.min_z = min(self.p1.z,self.p2.z,self.p3.z)
        #optionally, calculate the normal and compare it to the normal in the poly, if there is one
	        
