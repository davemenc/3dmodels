'this file reads a 3d stl file and then writes it back out to test stl write'
import time
from vector import Vector
from polygon import Polygon
from solid import Solid



        
def main():
    ######### DEFINE CONSTANTS #########
    inname = "dave_processed.stl"
    #inname = "cube.stl"
    #inname = "asymetric_rotated_cube.stl"
    #inname = "rotated_dave.stl"
    #inname = "rotated_cube.stl"
    print "1 *** Start: {} at seconds (file: {})".format( round(time.clock(),3),inname)
    outname = "test_out.stl"

    outdata = open(outname,"wt")
    
    ########### READ STL FILE ########
    solid = Solid()
    solid.read_stl_file(inname)
    print "2 *** File {1} parse complete at {0} seconds".format( round(time.clock(),3),inname)
    
    solid.write_stl_file(outname,inname)
    
     
    print "3 *** Exit: at {} seconds".format( round(time.clock(),3))
    
    
    
if __name__ == "__main__":
    main()    
