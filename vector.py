class Vector:
    """ Point class represents and manipulates x,y,z coords. """

    def __init__(self,x=None,y=None,z=None):
        """ Create a new point at the origin """
        if x is not None:
            self.x = float(x)
        if y is not None:
            self.y = float(y)
        if z is not None:
            self.z = float(z)
           
    def string(self):
            return "Vector: {0}, {1}, {2}".format(self.x,self.y,self.z)

    def dot(self,b):
    #""" self & b are vectors; it reuturns a scalar
            return self.x*b.x+self.y*b.y+self.z*b.y

    def normalise(self):
    # self is a vector; 
            return self/sqrt(dot(self,self))
            
    def cross(self,b):
    #self and b are both vectors; it returns a vector
            #(a2b3  -   a3b2,     a3b1   -   a1b3,     a1b2   -   a2b1)
            x = self.y * b.z - self.z * b.y
            y = self.z * b.z - self.x * b.z
            z = self.x * b.y - self.y * b.x 
            result = Vector(x,y,z)
            return result



                   
