NOTES
2dto3d.py - code (experimental at this point) to create 3d mesh out of 2d image
2dto3d.txt - explanation of how, exactly, I plan to do that
FreeCAD.exe - Shortcut.lnk - link to freecad
freecadmesh.txt - some notes about how to use frecadmesh workbench with python
images - directory with images
mesh - directory with code to read and write STL files
meshes - directory with files that contain meshes
MeshTestsApp.py - test code for mesh library in freecad; kind of a manual for things you can do 
notes.txt - this note file
pixels.py - file to read image files and convert them into raw pixel lists in tsv (tab separated values) files
workflow.txt - how to go from an image in PNG format to a 3d stl file (don't know how yet but I'll fill it in)
