#gen_height_map.py
from pixels import Pixels 
ofname = "D:/Making/3D/scad/surface2.dat"
pfname = "images/newtiki.png"
#pfname = "images/tiki_100.png"
#pfname = "images/rbghead.jpg"
#pfname = "images/rbghead_gray.png"
p = Pixels(pfname)
print(f"from {p.fname} in mode {p.mode} we get{p.width}X{p.height}; maxval:{p.maxval}; minval: {p.minval} ")
threshold = 0
yes = 3
no = 0
with open(ofname,"wt") as f:
	for y in range(p.height):
		for x in range(p.width):
			pix = p.fetch_pixel(x,y)
			val = no
			if pix>threshold:
				val = yes
			f.write(f"{val} ")
		f.write("\n")



#{self.pixels}

