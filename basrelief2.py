import random
import time
from vector import Vector
from polygon import Polygon
from solid import Solid
from PIL import Image

def tokenize(s):
    s = s.strip()
    stoks = s.split(" ")
    ttoks = []
    for t in range(0,len(stoks)):
        stoks[t] = stoks[t].strip()
        if len(stoks[t])>0:
            ttoks.extend(stoks[t].split("\t"))
    rtoks = []
    for t in range(0,len(ttoks)):
        ttoks[t]=ttoks[t].strip()
        if len(ttoks[t])>0:
            rtoks.append(ttoks[t])
    return rtoks

def concat_list(l):
    r=""
    for s in l:
       r += s+" "
    return r

def str_to_float(s):
    return float(s)

def parseline(line,state):
    #print line,state
    # check for blank line
    if len(line)<3:
        return state

    # tokenize the line
    toks = tokenize(line)
    #if we don't have the name of the solid, look for it
    if not state['solid']:
        if toks[0].lower()=='solid':
            del(toks[0])
            state['sname']=concat_list(toks)
            state['solid'] = True
            state['object'] = Solid(state['sname'])
            return state
        else:
            return state

    #if we don't have the facet keyword, look for it
    if not state['facet']:
        if toks[0].lower()== 'facet':
            state['facet'] = True
            if toks[1].lower() == 'normal': # we have normal info
                normal = Vector(str_to_float(toks[2]),str_to_float(toks[3]),str_to_float(toks[4]))
                state['poly']=Polygon(None,None,None,normal)
                
            return state
        else:
            return state

    #if we don't have the outer loop  keywords, look for it
    if not state['outer']:
        if toks[0].lower()=='outer':
            state['outer'] = True
        else:
            return state

    #if we don't have the vertexes
    if state['vertex'] < 3:
        if toks[0].lower() == 'vertex':
            state['poly'].add_vertex(Vector(str_to_float(toks[1]),str_to_float(toks[2]),str_to_float(toks[3])))
            state['vertex'] += 1
            return state
        else:
            return state

    #if we have the endloop, consume it
    if toks[0] == 'endloop':
        return state
    #if we have endfacet, we're done with this facet
    if toks[0] == 'endfacet':
        state['facet'] = False
        state['outer'] = False
        state['vertex'] = 0
        state['poly'].calculate()
        state['object'].add_facet(state['poly'])
    return state

def cvt_pix_2_coord(pix,min_coord,inc):
    """#need a way to convert a h,v into an x,y  # x = float(h)*horizontal_inc+minx"""
    return float(pix) * inc + min_coord

def cvt_coord_2_pix(coord,min_coord,inc):
    """ # need a way to convert a raw x,y to a pix# in h and v  # h = int((x-minx)/horizontal_inc) """
    return int((coord - min_coord)/inc)

        
def main():
    print "Start: {} seconds".format( round(time.clock(),3))
    MAXLINES = 200000
    RAY_HORIZONTAL = 1000#count of rays in horizontal
    RAY_VERTICAL  = 1000 #count of rays in vertical
    #inname = "dave_processed.stl"
    #inname = "cube.stl"
    inname = "rotated_cube.stl"
    outdataname = "testdata.txt"
    outstlname = "test.stl"
    outimage = inname+"2.jpg"
    
    state = {'solid':False,'facet':False,'outer':False,'vertex':0,'sname':""}
    print "Input STL: {0}\nOutput Data: {1}\nOutput STL: {2} ({4} lines)\nOutput Image: {3}\nResolution: {5}x{6}".format(inname,outdataname,outstlname,outimage,MAXLINES,RAY_HORIZONTAL,RAY_VERTICAL)

    linecount = 0
    with open(inname, 'rt') as f:
        for line in f:
            state = parseline(line,state)
            #print linecount,line,state
            linecount += 1
            if linecount > MAXLINES:
                break

    print "\n\n************** \n\n File parse complete {0} lines, {1} seconds".format(linecount, round(time.clock(),3))


######################## FILE HAS BEEN READ ########################
    #print state['object'].string()
    solid = state['object']
    maxx = solid.min_max['maxx']
    minx = solid.min_max['minx']
    maxy = solid.min_max['maxy']
    miny = solid.min_max['miny']
    minz = solid.min_max['minz']
    maxz = solid.min_max['maxz']
    z_range = abs(minz-maxz)
    print "z_range:{}".format(z_range)

    horizontal_inc = abs(maxx-minx)/RAY_HORIZONTAL
    vertical_inc = abs(maxy-miny)/RAY_VERTICAL
    minh = 0
    maxh = int((maxx-minx)/horizontal_inc)
    minv = 0
    maxv = int((maxy-miny)/vertical_inc)
    print "minh:{},maxh: {}, minv: {}, maxv: {}, R_H: {}, R_V: {}".format(minh,maxh, minv,maxv,RAY_HORIZONTAL,RAY_VERTICAL)

    print "width: {}, Height: {}, rayh:{}, rayv: {} hinc: {}, vinc: {}".format(abs(maxx-minx),
                                                                               abs(maxy-miny),
                                                                               RAY_HORIZONTAL,
                                                                               RAY_VERTICAL,
                                                                               horizontal_inc,
                                                                               vertical_inc)

    heights = [[minz for x in range(RAY_VERTICAL+1)] for y in range(RAY_HORIZONTAL+1)]
    #print "MINZ=",minz
    for facet in solid.facet_list:
        f_minh = int((facet.min_x-minx)/horizontal_inc)
        f_maxh = int((facet.max_x-minx)/horizontal_inc)+1
        if f_maxh > maxh:
            f_maxh = maxh
        f_minv = int((facet.min_y-miny)/vertical_inc)
        f_maxv = int((facet.max_y-miny)/vertical_inc)+1
        if f_maxv > maxv:
            fmaxv = maxv
        #print "\n______\nfacet: {0}\nf_minh: {1},f_maxh: {2}, f_minv: {3}, f_maxv: {4}\n".format(facet.string(),f_minh,f_maxh,f_minv,f_maxv)
        for h in range(f_minh,f_maxh):
            for v in range(f_minv,f_maxv):
                x = cvt_pix_2_coord(h,minx,horizontal_inc)#float(h)*horizontal_inc+minx
                y = cvt_pix_2_coord(v,miny,vertical_inc)# y = float(v)*vertical_inc+miny
                if not facet.ray_in_poly(x,y):
                    z = minz
                else:
                    z = facet.calcZ(x,y)
                    #print "{0},{1}:{2} was {3}".format(h,v,z, heights[h][v])
                if z > heights[h][v]:
                    heights[h][v] = z
                #print "\th,v: {0},{1}; x,y: {2},{3}; z: {4}".format(h,v,x,y,z)
    print "Done: {} seconds".format( round(time.clock(),3))
    outdata = open(outdataname,"wt")
    hwide = len(heights)
    hhigh = len(heights[0])
    
    #print "hwide: {}; hhigh: {}".format(hwide,hhigh)
    minh = 2e200
    maxh = -2e200
    
    for h in range(0,len(heights)):
        for v in range(0,len(heights[h])):
            if heights[h][v] > maxh:
                maxh = heights[h][v]
            if heights[h][v] < minh: 
                minh = heights[h][v]
    
    r = abs(minh-maxh)          
    print minh,maxh,r
    #print "Minh,maxh: {0},{1}; range: {2}".format(minh, maxh,r)
    pix = []
    for h in range(0,len(heights)):
        for v in range(0,len(heights[h])):
            z = heights[v][h]
            gray = int(256*(float(z-minh)/float(r)))
            pix.append((gray,gray,gray))
            outdata.write("%3d" % gray)
            outdata.write(" ")
        outdata.write("\n")

    #print "heights",heights
    outdata.close()
    print "File Closed: {} seconds".format( round(time.clock(),3))

    im2 = Image.new("RGB",(RAY_HORIZONTAL+1,RAY_VERTICAL+1))
    im2.putdata(pix)
    im2.save(outimage,"JPEG")
    #im2.show()
    print "exit: {} seconds".format( round(time.clock(),3))
    
    
    
if __name__ == "__main__":
    main()    
