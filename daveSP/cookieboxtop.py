#! /usr/bin/env python3
import sys

from solid import scad_render_to_file
from solid.objects import cube, cylinder, hole, part, rotate,difference, translate, union,hull,color
from solid.utils import FORWARD_VEC, right, up,down,left
from solid import scad_render_to_file

SEGMENTS = 120
def round_box(width,height,round):
	return hull()(
	translate((width-round,round,0))(cylinder(r=round,h=height))+
	translate((round,width-round,0))(cylinder(r=round,h=height))+
	translate((round,round,0))(cylinder(r=round,h=height))+
	translate((width-round,width-round,0))(cylinder(r=round,h=height)))
def old ():
	actual = 130.7
	offby = actual - box_w

	i_corner_offset = box_w-inner_d/2-8.75
	i_corner1 = translate((0, 0, 0))(cylinder(d=inner_d, h=lip_h, center=False))
	i_corner2 = translate((i_corner_offset, 0, 0))(cylinder(d=inner_d, h=lip_h, center=False))
	i_corner3 = translate((0, i_corner_offset, 0))(cylinder(d=inner_d, h=lip_h, center=False))
	i_corner4 = translate((i_corner_offset, i_corner_offset, 0))(cylinder(d=inner_d, h=lip_h, center=False))
	inner = translate((outer_d/2-wall/2, outer_d/2-wall/2, 0))(hull()(translate((0, 0, wall))(i_corner1+i_corner2+i_corner3+i_corner4)))
	print(i_corner_offset, actual,offby)
	o_corner_offset = box_w-outer_d/2+wall
	o_corner1 = translate((0, 0, 0))(cylinder(d=outer_d, h=lip_h+wall, center=False))
	o_corner2 = translate((o_corner_offset, 0, 0))(cylinder(d=outer_d, h=lip_h, center=False))
	o_corner3 = translate((0, o_corner_offset, 0))(cylinder(d=outer_d, h=lip_h, center=False))
	o_corner4 = translate((o_corner_offset, o_corner_offset, 0))(cylinder(d=outer_d, h=lip_h, center=False))
	outer = hull()(o_corner1+o_corner2+o_corner3+o_corner4)

if __name__ == '__main__':
	wall = 3
	lip_h = 9.5
	top_h = wall+lip_h
	inner_d = 17.9
	outer_d = inner_d+wall
	box_w=127.75
	out_dir = sys.argv[1] if len(sys.argv) > 1 else None

	inner = translate((wall, wall, wall))(round_box(box_w,top_h,inner_d/2))
	outer = translate((0,0,0))(round_box(box_w+wall*2,top_h,outer_d/2))


	box_lid =  color("blue")(outer)-color("red")(inner)
	file_out = scad_render_to_file(box_lid, out_dir=out_dir, file_header=f'$fn = {SEGMENTS};', include_orig_code=True)
	print(f"{__file__}: SCAD file written to: \n{file_out}")
