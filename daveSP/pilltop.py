#! /usr/bin/env python3
import sys

from solid import scad_render_to_file
from solid.objects import *
from solid.utils import *
from solid.screw_thread import *

SEGMENTS = 120
out_dir = sys.argv[1] if len(sys.argv) > 1 else None

# OLD DIMENsIONS
oldbox =91.14
lid =91.42
diff = .28
lid_space= .3
center= 16
screw= 7.6
height = 19.43
wall =3
# NEW DIMENSIONS
new_d = 100
new_h = 20
newscrew=9
topspace = 4
top_h = 5+wall
top_center=9
top_w = new_d+topspace
top= difference()(
		cylinder(d=top_w+wall, h=top_h, center=False),
		translate((0, 0, wall))(cylinder(d=top_w-wall, h=top_h, center=False)),
	)

center_hole = cylinder(d=top_center,h=new_h,center=True)

center_piece = translate((0,0,-1))(cylinder(d=top_center+wall*2.5,h=new_h+2,center=False))

block =	cylinder(d=new_d, h=new_h, center=False) - translate([-50,0,-1])(cube([100,100,22]))-translate([0+30,-141+30,-1])(rotate([0,0,45])(cube([100,100,22])))
block= translate((1.1,.6,-wall))(block-center_piece)
hole=translate((0,0,wall))(cylinder(d=newscrew,h=new_h))

# Adding the file_header argument as shown allows you to change
# the detail of arcs by changing the SEGMENTS variable.  This can
# be expensive when making lots of small curves, but is otherwise
# useful.

file_out = scad_render_to_file(top-block-center_hole, out_dir=out_dir, file_header=f'$fn = {SEGMENTS};')
print(f"{__file__}: SCAD file written to: \n{file_out}")

