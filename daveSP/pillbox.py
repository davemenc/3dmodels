#! /usr/bin/env python3
import sys

from solid import scad_render_to_file
from solid.objects import *
from solid.utils import *
from solid.screw_thread import *

SEGMENTS = 120
out_dir = sys.argv[1] if len(sys.argv) > 1 else None

# OLD DIMENsIONS
oldbox =91.14
lid =91.42
diff = .28
lid_space= .3
center= 16
screw= 7.6
height = 19.43
wall =3
# NEW DIMENSIONS
new_d = 100
new_h = 20
newscrew=9
topspace = .6;

def assembly():
	section = screw_thread.default_thread_section(tooth_height=10, tooth_depth=5)
	s = screw_thread.thread(outline_pts=section,
							inner_rad=inner_rad,
							pitch=screw_height,
							length=screw_height,
							segments_per_rot=SEGMENTS,
							neck_in_degrees=90,
							neck_out_degrees=90)

	c = cylinder(r=inner_rad, h=screw_height)
	return s + c

case= difference()(
		cylinder(d=new_d, h=new_h, center=False),
		translate((0, 0, wall))(cylinder(d=new_d-wall*2, h=new_h, center=False))
	)

center = cylinder(d=center,h=new_h,center=False)

divider=translate((-wall/2,-new_d/2,0))((cube([wall,new_d,new_h])))
dividers = divider
for angle in [45,90,135]:
	dividers += rotate(a=angle, v=UP_VEC)(divider)

block =	cylinder(d=new_d, h=new_h, center=False) - translate([-50,0,-1])(cube([100,100,22]))-translate([0+30,-141+30,-1])(rotate([0,0,45])(cube([100,100,22])))

hole=translate((0,0,wall))(cylinder(d=newscrew,h=new_h))

# Adding the file_header argument as shown allows you to change
# the detail of arcs by changing the SEGMENTS variable.  This can
# be expensive when making lots of small curves, but is otherwise
# useful.
file_out = scad_render_to_file((case+center+dividers+block)-hole, out_dir=out_dir, file_header=f'$fn = {SEGMENTS};')
print(f"{__file__}: SCAD file written to: \n{file_out}")

top_file = scad_render_to_file((case+center+dividers+block)-hole, out_dir=out_dir, file_header=f'$fn = {SEGMENTS};')
